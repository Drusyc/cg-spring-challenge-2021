import sys
import math
from datetime import time
from enum import Enum
from typing import Tuple, Any, Union
from operator import attrgetter

# https://youtu.be/gZMdOiqchDk
# https://www.codingame.com/ide/challenge/spring-challenge-2021

# To debug: print("Debug messages...", file=sys.stderr, flush=True)

DEBUG_PRINT = 0

# BRONZE_LEAGUE LASTS 24 DAYS
LAST_DAY = 24

INDEX_CELLS_CIRCLE_1 = [0, 1, 2, 3, 4, 5, 6]
INDEX_CELLS_CIRCLE_2 = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
INDEX_CELLS_CIRCLE_3 = [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]

INDEX_CELLS_CIRCLE_ARR = [
    INDEX_CELLS_CIRCLE_1,
    INDEX_CELLS_CIRCLE_2,
    INDEX_CELLS_CIRCLE_3
]

# ACTION
ACTION_CHOP = "COMPLETE"
ACTION_GROW = "GROW"
ACTION_SEED = "SEED"
ACTION_WAIT = "WAIT"

DIRECTION_DROITE = 0
DIRECTION_HAUT_DROITE = 1
DIRECTION_HAUT_GAUCHE = 2
DIRECTION_GAUCHE = 3
DIRECTION_BAS_GAUCHE = 4
DIRECTION_BAS_DROITE = 5

SEED_SIZE = 0

FLAG_ME = 1
FLAG_OPP = 0

COST_TO_CHOP = 4
COST_TO_GROW_SIZE_1 = 1  # from seed to size 1
COST_TO_GROW_SIZE_2 = 3
COST_TO_GROW_SIZE_3 = 7

COST_ACTION_ON_TREE = [
    COST_TO_GROW_SIZE_1,  # [0] seed -> 1
    COST_TO_GROW_SIZE_2,  # [1] 1 -> 2
    COST_TO_GROW_SIZE_3  # [2] 2 -> 3
]


class ActionType(Enum):
    WAIT = "WAIT"
    SEED = "SEED"
    GROW = "GROW"
    COMPLETE = "COMPLETE"


class Action:
    def __init__(self, type, target_cell_id=None, origin_cell_id=None):
        self.type = type
        self.target_cell_id = target_cell_id
        self.origin_cell_id = origin_cell_id

    def __str__(self):
        if self.type == ActionType.WAIT:
            return 'WAIT'
        elif self.type == ActionType.SEED:
            return f'SEED {self.origin_cell_id} {self.target_cell_id}'
        else:
            return f'{self.type.name} {self.target_cell_id}'

    @staticmethod
    def parse(action_string):
        split = action_string.split(' ')
        if split[0] == ActionType.WAIT.name:
            return Action(ActionType.WAIT)
        if split[0] == ActionType.SEED.name:
            return Action(ActionType.SEED, int(split[2]), int(split[1]))
        if split[0] == ActionType.GROW.name:
            return Action(ActionType.GROW, int(split[1]))
        if split[0] == ActionType.COMPLETE.name:
            return Action(ActionType.COMPLETE, int(split[1]))

    @staticmethod
    def wait():
        return f'{ACTION_WAIT} where is my sun!'


class BoardCell:

    def __init__(self,
                 cell_ind,
                 cell_rich,
                 cell_neigh_0,
                 cell_neigh_1,
                 cell_neigh_2,
                 cell_neigh_3,
                 cell_neigh_4,
                 cell_neigh_5
                 ):
        self.index = cell_ind
        self.richness = cell_rich
        self.neigh_0 = cell_neigh_0  # index voisin DIRECTION_DROITE = 0
        self.neigh_1 = cell_neigh_1  # index voisin DIRECTION_HAUT_DROITE = 1
        self.neigh_2 = cell_neigh_2  # index voisin DIRECTION_HAUT_GAUCHE = 2
        self.neigh_3 = cell_neigh_3  # index voisin DIRECTION_GAUCHE = 3
        self.neigh_4 = cell_neigh_4  # index voisin DIRECTION_BAS_GAUCHE = 4
        self.neigh_5 = cell_neigh_5  # index voisin DIRECTION_BAS_DROITE = 5

    def is_usable(self):
        # 0 pour les cases inutilisables. Rien ne peut pousser sur ces cases
        return self.richness != 0

    def get_neigh(self, relative_position: int) -> int:
        """
        :return:
            l'index de la case voisine, -1 si il n'y a pas de case voisine.
        """
        if relative_position == -1:
            return -1

        return self.__getattribute__(f"neigh_{relative_position}")

    def get_all_voisins(self):
        return [self.get_neigh(x) for x in range(6)]


class Tree:

    def __init__(self,
                 tree_cell_index: int,
                 tree_cell_obj: BoardCell,
                 tree_size: int,
                 tree_cell_richness: int,
                 tree_points_if_chopped: int,
                 tree_is_dormant: int,
                 tree_is_mine: int):
        self.cell_index = tree_cell_index
        self.cell_obj = tree_cell_obj
        self.size = tree_size
        self.richness = tree_cell_richness
        self.points_if_chopped = tree_points_if_chopped  # Vous ne pouvez compléter le cycle de vie que des arbres de taille 3
        self.is_dormant = tree_is_dormant
        self.cost_to_grow = -1
        self.interest_to_grow = -1  # growing interest!
        self.is_shaded_today = False
        self.is_shaded_tomorrow = False
        self.is_mine = tree_is_mine  # 1 if this is your tree

    def growing_interest(self):
        return self.richness ** 2 + (self.size + 1) - (self.is_shaded_tomorrow * 3)

    def set_cost_to_grow(self, val: int):
        self.cost_to_grow = val

    def set_interest(self, val: int):
        self.interest_to_grow = val

    def is_seed(self):
        return self.size == 0

    def shadow_size(self):
        return self.size


class Forest:

    def __init__(self):
        self.all_trees = []
        self.trees_3 = []
        self.trees_2 = []
        self.trees_1 = []
        self.trees_0 = []  # seeds

    def __len__(self):
        return len(self.all_trees)

    def get_list_by_size(self, tree_size: int) -> list:
        return self.__getattribute__(f"trees_{tree_size}")

    def add(self, t: Tree):
        self.all_trees.append(t)
        self.get_list_by_size(t.size).append(t)

    def get(self, ind: int) -> Tree:
        return self.all_trees[ind]

    def count_tree_of_size(self, tree_size: int):
        return len(self.get_list_by_size(tree_size))

    def get_max_based_on_attr(self, tree_size: int, attr: str) -> Tree:
        tree_list = self.get_list_by_size(tree_size)
        return max(tree_list, key=attrgetter(attr))

    def cost_to_grow(self, current_tree_size: int):
        """
        Faire pousser un arbre dépend de sa taille ET des autres arbres de la même taille+1
        """
        if current_tree_size == 3:
            # should not happen
            return -1

        desired_sized = current_tree_size + 1

        # compte le nombre d'arbre de la taille souhaitée
        nb_trees_of_same_size = self.count_tree_of_size(desired_sized)

        return COST_ACTION_ON_TREE[current_tree_size] + nb_trees_of_same_size

    def cost_to_seed(self):
        """
            vous devez dépenser un nombre de points de soleil égal au nombre de graines
            (arbres de taille 0) présentes dans la forêt que vous possédez.
        """
        return self.count_tree_of_size(tree_size=SEED_SIZE)

    def best_tree_to_grow_wood(self, sun_points: int):
        """
            Récupère le meilleur arbre à faire pousser de tree.interest.
            Qui n'est pas dormant.
            Si le nombre de points de sun n'est pas assez élevé pour faire pousser le meilleur arbre.
            Get le second poussable.
        """
        cell_ind = -1
        max_p = -1
        for ind in range(len(self.all_trees)):
            tr = self.get(ind)
            if tr.is_dormant or tr.size == 3:
                continue

            p = tr.interest_to_grow
            cost = tr.cost_to_grow
            if p > max_p and sun_points - cost >= 0:
                cell_ind = tr.cell_index
                max_p = p

        return cell_ind, max_p

    def best_tree_to_grow(self, sun_points: int) -> Union[Tree, None]:
        """
            Parcours les arbres, du plus grand au plus petits.
            Récupère l'arbre qui peut être GROW (si assez de 'sun_points).
            Les arbres sont triés par taille puis par TREE.INTEREST
        """
        for tr_size in reversed(range(3)):

            print(f"Best to_grow size : '{tr_size}' "
                  f"cost: '{self.cost_to_grow(current_tree_size=tr_size)}'", file=sys.stderr, flush=True)

            if self.count_tree_of_size(tree_size=tr_size) == 0:
                continue

            if sun_points >= self.cost_to_grow(current_tree_size=tr_size):
                # on a assez de points pour faire pousser un arbre de taille 'tr_size' en 'tr_size+1'
                # Récupère l'arbre qui a le meilleur investissemtn à faire pousser

                # best_tree_to_grow = sorted(l_tree, key=lambda x: x.interest_to_grow, reverse=True)[0]
                best_tree_to_grow = self.get_max_based_on_attr(tree_size=tr_size, attr='interest_to_grow')

                print(f"Best_to_grow size: '{tr_size}'", file=sys.stderr, flush=True)
                print(f"Best_to_grow tree: '{best_tree_to_grow.cell_index}'", file=sys.stderr, flush=True)

                return best_tree_to_grow

        return None

    def best_tree_to_chop(self) -> Tree:
        """
            Récupère l'arbre de taille 3 qui maximise 'points_if_chopped'
        """
        return self.get_max_based_on_attr(tree_size=3, attr='points_if_chopped')

    def get_best_tree_interest(self):
        # return the tree that gives the more points

        cell_ind = -1
        max_p = -1
        for ind in range(len(self.all_trees)):
            p = self.all_trees[ind].interest_to_grow
            if p > max_p:
                cell_ind = self.all_trees[ind].cell_index
                max_p = p

        return cell_ind, max_p


def compute_sun_direction(d: int = 0) -> int:
    return d % 6


class Board:

    def __init__(self):
        self.list_cells = []
        self.list_index_occupied_cells = []  # list d'index de cells occupées
        self.today = -1
        self.nutrients = -1
        self.friendly_forest = None  # my forest
        self.opp_forest = None  # opponent's forest
        self.possible_actions = []
        self.current_tree_to_grow = -1  # indice de l'arbre à grow absolument to level 3

    def set_today(self, today_day_is_today: int):
        self.today = today_day_is_today

    def add(self, bo: BoardCell) -> None:
        self.list_cells.append(bo)

    def add_occupancy(self, ind: int) -> None:
        self.list_index_occupied_cells.append(ind)

    def get_cell(self, ind: int) -> BoardCell:
        return self.list_cells[ind]

    def set_nutrients(self, val: int) -> None:
        self.nutrients = val

    def set_friendly_forest(self, f: Forest):
        self.friendly_forest = f

    def set_opp_forest(self, f: Forest):
        self.opp_forest = f

    def cell_info(self, ind: int) -> Union[tuple[BoardCell, Tree], tuple[BoardCell, None]]:
        """
        Check si un object est sur la cellule
        return BordCell, Tree (=None is no tree)
        """
        cell_obj = self.get_cell(ind)

        for tr in self.friendly_forest.all_trees:
            if tr.cell_index == ind:
                return cell_obj, tr

        for tr in self.opp_forest.all_trees:
            if tr.cell_index == ind:
                return cell_obj, tr

        return cell_obj, None

    def count_number_neight_obj(self, ind: int, circle_size=1):
        """
        Compte le nombre d'arbres autour de la cellule
        """

        target_cell = self.get_cell(ind)
        count = 0

        neights = target_cell.get_all_voisins()

        for cell in neights:
            _, t = self.cell_info(cell)

            if t is not None:
                count += 1

        return count


def reachable_cells_to_seed(tr: Tree, board: Board) -> set:
    """
    SEED: Commandez à un arbre de lancer une graine sur une case
    dont la distance à l'arbre est inferieure ou égal à la taille de l'arbre.

    Renvoie toutes les cellules concernées, qu'elles soient occupées ou non.
    Renvoie une liste d'index (pas une liste de CellBoard)
                              (pour récupérer la cellule => game_board.get(cell_index))
    """
    set_of_cells = set()
    tree_position = tr.cell_index
    tree_size = tr.size

    list_cell_to_check = [tree_position]
    for ind in range(tree_size):

        tmp = set()
        for cell_ind in list_cell_to_check:
            cell_obj = board.get_cell(cell_ind)
            tmp.update(cell_obj.get_all_voisins())

        list_cell_to_check = list(tmp)
        set_of_cells.update(tmp)

    set_of_cells.discard(tree_position)
    set_of_cells.discard(-1)
    return set_of_cells


def best_to_seed_around_tree(tr: Tree, board: Board) -> Union[BoardCell, None]:
    """
        Cherche le meilleur endroit, accessible, pour SEED autour de Tree.
        Se base sur la richness des cellules
    """
    if tr.is_dormant or tr.size == SEED_SIZE:
        return None

    reachable_cells_index = reachable_cells_to_seed(tr, board)

    # on enlève les cellules occupées
    reachable_cells_index.difference_update(board.list_index_occupied_cells)

    # on enlève les cellules avec richness == 0
    reachable_cells = []

    for ind in reachable_cells_index:
        cell = board.get_cell(ind)

        if cell.richness > 0:
            reachable_cells.append(cell)

    # si l'arbre est complètement entouré ou richness==0, aucune cellule:
    if len(reachable_cells) == 0:
        return None

    return max(reachable_cells, key=attrgetter('richness'))


def best_to_seed(f: Forest, board: Board) -> tuple[Union[Any, Any], Union[Tree, BoardCell]]:
    """
        Cherche le meilleur endroit, accessible, pour SEED autour de Tree.
        Se base sur la richness des cellules
    """

    best_tree = -1
    best_cell_to_seed = -1
    best_richness = -1

    for tr in f.all_trees:
        if tr.size == SEED_SIZE or tr.is_dormant:
            continue

        # peut renvoyé None si la cellule est complétement entourée
        current_best_cell = best_to_seed_around_tree(tr, board)
        if current_best_cell is not None:
            print(f"BEST: Tree: '{tr.cell_index}' -> Cell_index: {current_best_cell.index}"
                  f" -> Cell_richness: {current_best_cell.richness}", file=sys.stderr, flush=True)
        else:
            print(f"BEST: Tree: '{tr.cell_index}' -> None", file=sys.stderr, flush=True)

        if current_best_cell is not None and current_best_cell.richness > best_richness:
            best_tree = tr
            best_cell_to_seed = current_best_cell
            best_richness = current_best_cell.richness

    return best_tree, best_cell_to_seed


def best_to_seed_from_list(actions_list: list, board: Board, circle: int = -1) -> Union[None, Action]:
    """

    """
    if len(actions_list) == 0:
        return None

    # Si on veut planter quelque part en spécifique
    index_desired_cells_arr = []
    if circle != -1:
        index_desired_cells_arr = INDEX_CELLS_CIRCLE_ARR[circle]

    actions_ok = []

    for act in actions_list:
        target = act.target_cell_id
        # origin = act.origin_cell_id

        if circle != -1:
            if target not in index_desired_cells_arr:
                continue

        # vérifie si cette case a des voisins
        target_cell = board.get_cell(target)
        nb_neights = board.count_number_neight_obj(target)

        value = target_cell.richness - (nb_neights ** 2)

        actions_ok.append((act, value))

        print(f"Best actions seeds{str(act)} VALUE: {value}", file=sys.stderr, flush=True)
    best_action, _ = max(actions_ok, key=lambda x: x[1])

    return best_action


def get_cells_shaded_by_tree(t: Tree,
                             board: Board,
                             sun_dir: int) -> list:
    # la numérotation des cases correspond aux changements de direction du soleil
    # Premier jour: le soleil pointe à droite. Shadow sur les cases 0 (cell_neigh_0). today%6==0
    # Deuxième jour: le soleil pointe en haut à droite. Shadow sur la case 1. today%6==1
    # Donc today%6 donne la case voisine ombrée par l'arbre de la case actuelle.

    if t.is_seed():
        return []

    t_size = t.size
    current_cell_obj = board.get_cell(t.cell_index)

    shaded_cells = []
    ind = 0

    # print(f"****shaded_function DEBUG ****", file=sys.stderr, flush=True)
    # print(f"**** Tree:cell:{t.cell_index} / size:{t_size} ****", file=sys.stderr, flush=True)
    # print(f"**** Sun_direction{sun_direction} ****", file=sys.stderr, flush=True)
    while ind < t_size:
        # récupère le voisin pointé par la direction du soleil (sun_dir)
        cell_neigh_index = current_cell_obj.get_neigh(sun_dir)
        # print(f"**** ind: voisin shaded: {cell_neigh_index} ****", file=sys.stderr, flush=True)

        if cell_neigh_index == -1:
            break

        shaded_cells.append(cell_neigh_index)
        current_cell_obj = board.get_cell(cell_neigh_index)
        ind += 1

    return shaded_cells


def set_shaded_info(t: Tree, board: Board) -> None:
    """
    Est-ce que l'arbre est à l'ombre / va à être à l'ombre les prochains jours ?
    during_x_days==1 => aujourd'hui
    during_x_days==2 => demain..
    """
    tr_size = t.size
    today = board.today

    # taille max d'une ombre : 3
    for ind in range(0, 2):
        sun_dir = compute_sun_direction(today + ind)
        tmp_cell_obj = t.cell_obj
        for j in range(3):
            neigh_ind = tmp_cell_obj.get_neigh((sun_dir + 3) % 6)
            cell, potential_tree = board.cell_info(neigh_ind)

            if potential_tree is not None and potential_tree.size >= tr_size:
                if ind == 0:
                    t.is_shaded_today = True
                elif ind == 1:
                    t.is_shaded_tomorrow = True

            tmp_cell_obj = cell
    return


def get_chop_actions(board: Board):
    # a-t-on assez de point pour chop ?
    return [x for x in board.possible_actions if x.type == ActionType.COMPLETE]


def get_seed_actions(board: Board):
    # a-t-on assez de point pour seed ?
    return [x for x in board.possible_actions if x.type == ActionType.SEED]


def get_grow_actions(board: Board):
    return [x for x in board.possible_actions if x.type == ActionType.GROW]


def pretty_forest_print(f: Forest):
    list_all = [x.cell_index for x in f.all_trees]
    list_3 = [x.cell_index for x in f.trees_3]
    list_2 = [x.cell_index for x in f.trees_2]
    list_1 = [x.cell_index for x in f.trees_1]
    list_0 = [x.cell_index for x in f.trees_0]

    print(f"All Trees:{list_all}", file=sys.stderr, flush=True)

    if len(list_3) > 0:
        print(f"Trees Size 3:{list_3}", file=sys.stderr, flush=True)

    if len(list_2) > 0:
        print(f"Trees Size 2:{list_2}", file=sys.stderr, flush=True)

    if len(list_1) > 0:
        print(f"Trees Size 1:{list_1}", file=sys.stderr, flush=True)

    if len(list_0) > 0:
        print(f"Trees Size 0:{list_0}", file=sys.stderr, flush=True)


def prettree_print(t: Tree,
                   board: Board,
                   sun_dir: int):
    # pretty print pour un Tree
    todict = vars(t)
    voisins = t.cell_obj.get_all_voisins()
    shaded_cells = get_cells_shaded_by_tree(t, board, sun_dir)
    print(f"Tree => {todict}", file=sys.stderr, flush=True)
    print(f"     => Voisins:{voisins}", file=sys.stderr, flush=True)
    print(f"     => Shaded:{shaded_cells}", file=sys.stderr, flush=True)


def silver_grow(action_list_grow: list,
                board: Board,
                circle: int = -1,
                favorised_level_three: bool = False,
                pref_seed: bool = False):
    print(f"SilverGrow: Last Tree to Grow: {board.current_tree_to_grow}",
          file=sys.stderr, flush=True)

    if favorised_level_three and board.current_tree_to_grow != -1:
        # Alors on fait du 1 -> 2 -> 3 au maximum
        target_index = str(board.current_tree_to_grow)

        # on cherche si on peut le faire grandir
        for act in action_list_grow:
            if target_index in str(act):
                _, tr = board.cell_info(act.target_cell_id)
                if tr.size == 2:
                    board.current_tree_to_grow = -1
                return str(act)

    else:
        act = best_tree_to_grow_with_list(action_list=action_list_grow,
                                          board=board,
                                          circle=circle,
                                          pref_seed=pref_seed)
        if act is not None:
            _, tr = board.cell_info(act.target_cell_id)

            # on mémorise cet arbre
            if tr.size == 2:
                board.current_tree_to_grow = -1
            else:
                board.current_tree_to_grow = tr.cell_index

            return str(act)

    return Action.wait()


def silver_seed(action_list_seed: list, board: Board, circle: int = -1):
    act = best_to_seed_from_list(actions_list=action_list_seed, board=board, circle=circle)
    if act is not None:
        return str(act)

    return Action.wait()


def strategy_silver_2(board: Board,
                      my_forest: Forest,
                      sun_points: int):
    """
    -> grow
    -> seed
    -> chop

    # Deux premiers jours, pousser

    # Pas plus d'une graine à la fois

    # Sinon, pousser

    # Chop je sias pas quand

    """

    final_action = f"{ACTION_WAIT} where is my sun!"
    chop_actions = get_chop_actions(board)
    grow_actions = get_grow_actions(board)
    seed_actions = get_seed_actions(board)

    nb_graines = len(my_forest.trees_0)

    # Deux premiers jours, pousser les deux arbres
    if board.today <= 2:
        final_action = silver_grow(action_list_grow=grow_actions, board=board, circle=-1)
        if board.today == 2 and len(grow_actions) == 0:
            final_action = silver_seed(action_list_seed=seed_actions, board=board, circle=-1)

    elif board.today >= LAST_DAY - 3 and len(chop_actions) > 0:
        tr = my_forest.best_tree_to_chop()
        if tr is not None:
            final_action = f"{ACTION_CHOP} {tr.cell_index}"

    elif not board.today < 12 and len(chop_actions) >= 3:
        tr = my_forest.best_tree_to_chop()
        if tr is not None:
            final_action = f"{ACTION_CHOP} {tr.cell_index}"

    elif len(grow_actions) > 0:
        final_action = silver_grow(action_list_grow=grow_actions,
                                   board=board, circle=-1,
                                   favorised_level_three=False,
                                   pref_seed=True)
        print(f"GROW: {final_action}", file=sys.stderr, flush=True)

    else:
        final_action = f"{ACTION_WAIT} where is my sun!"

    if not board.today <= 2 and ACTION_WAIT in final_action and nb_graines == 0 and board.today <= LAST_DAY - 7:
        final_action = silver_seed(action_list_seed=seed_actions, board=board, circle=-1)

    return final_action


def strategy_silver_1(board: Board,
                      my_forest: Forest,
                      sun_points: int):
    """
    -> grow
    -> seed
    -> chop

    # Deux premiers jours, pousser

    # Ensuite, planter au milieu

    # Garder 3 arbres lvl 3 pour énergies

    # Sinon seed / grow to max / chop

    """

    final_action = f"{ACTION_WAIT} where is my sun!"
    chop_actions = get_chop_actions(board)
    grow_actions = get_grow_actions(board)
    seed_actions = get_seed_actions(board)

    # Deux premiers jours, pousser les deux arbres
    if board.today <= 2:
        final_action = silver_grow(action_list_grow=grow_actions, board=board, circle=-1)
        if board.today == 2 and len(grow_actions) == 0:
            final_action = silver_seed(action_list_seed=seed_actions, board=board, circle=-1)

    elif board.today >= LAST_DAY - 2 and len(chop_actions) > 0:
        tr = my_forest.best_tree_to_chop()
        if tr is not None:
            final_action = f"{ACTION_CHOP} {tr.cell_index}"

    elif len(chop_actions) >= 3:
        tr = my_forest.best_tree_to_chop()
        if tr is not None:
            final_action = f"{ACTION_CHOP} {tr.cell_index}"

    elif len(grow_actions) > 0:
        final_action = silver_grow(action_list_grow=grow_actions, board=board, circle=-1, favorised_level_three=True)
        print(f"GROW: {final_action}", file=sys.stderr, flush=True)

        if ACTION_WAIT in final_action and len(seed_actions) > 0 and not board.today >= LAST_DAY - LAST_DAY / 4 and len(
                my_forest.trees_0) < 3:
            # si on wait mais qu'on pourra faire pousser le prochain, autant planter
            _, tree_to_grow = board.cell_info(board.current_tree_to_grow)
            cost_to_grow_this_tree = my_forest.cost_to_grow(tree_to_grow.size)
            print(f"Cost to grow the last {board.current_tree_to_grow} : {cost_to_grow_this_tree}",
                  file=sys.stderr, flush=True)
            cost_to_seed = my_forest.cost_to_seed()

            if cost_to_grow_this_tree + cost_to_seed <= (how_many_sun_tomorrow(board, my_forest) + sun_points):
                # alors on plante !
                final_action = silver_seed(action_list_seed=seed_actions, board=board, circle=-1)

    # Sinon seed
    # if can_we_seed(sun_point=sun_points, f=my_forest):
    elif len(seed_actions) > 0 and not board.today >= LAST_DAY - LAST_DAY / 4 and len(my_forest.trees_0) < 3:
        final_action = silver_seed(action_list_seed=seed_actions, board=board, circle=-1)

    else:
        final_action = f"{ACTION_WAIT} where is my sun!"

    return final_action


def best_tree_to_grow_with_list(action_list: list,
                                board: Board,
                                circle: int = -1,
                                pref_seed: bool = False) -> Union[None, Action]:
    """
        Best to grow avec la liste des actions possibles
        circle:
            -1 => osef
            1 => cercle vert milieu
            2 => cercle interieur
            3 => cercle exterieur
    """

    if len(action_list) == 0:
        return None

    action_ok = []

    # Si on veut planter quelque part en spécifique
    index_desired_cells_arr = []
    if circle != -1:
        index_desired_cells_arr = INDEX_CELLS_CIRCLE_ARR[circle]

    f = board.friendly_forest
    go_grow_seed = False
    if pref_seed:
        # si on peut faire pousser une seed + un petit autre
        # on préfère ça à en faire pousser qu'un grand
        prices_size = [f.cost_to_grow(trsize) for trsize in range(0, 4)]
        price_seed = prices_size[0]
        minprise = min(prices_size[1:])

        if (price_seed + minprise) <= my_sun_points:
            go_grow_seed = True

    for act in action_list:
        # print(f"ACTION {act}", file=sys.stderr, flush=True)
        target_index = act.target_cell_id

        # est-ce que c'est arbre est au bon endroit
        if circle != -1:
            if target_index not in index_desired_cells_arr:
                continue

        cell, tr = board.cell_info(target_index)

        # value = ((5 - tr.size) - (tr.is_shaded_tomorrow * 2)) / board.friendly_forest.cost_to_grow(tr.size)
        nb_neights = board.count_number_neight_obj(target_index)
        # classement 20
        value = (tr.size ** 2 + nb_neights - (tr.is_shaded_tomorrow * 2)) - board.friendly_forest.count_tree_of_size(tr.size)
        # value = (go_grow_seed and tr.size == SEED_SIZE) * 100
        # value += (tr.size ** 2 + nb_neights - (tr.is_shaded_tomorrow * 2)) - board.friendly_forest.count_tree_of_size(tr.size)

        action_ok.append((act, value))
        print(f"Best actions to grow {str(act)} VALUE: {value}", file=sys.stderr, flush=True)

    best_action = max(action_ok, key=lambda x: x[1])

    return best_action[0]


def how_many_sun_tomorrow(board: Board, my_forest: Forest):
    """
    compte le nombre de points que je vais sûrement gagner demain
    """
    count = 0
    for t in my_forest.all_trees:
        if not t.is_shaded_tomorrow:
            count += t.size

    return count


# Variable GLOBALES
game_board = Board()
sun_direction = -1

# THE GAME
number_of_cells = int(input())  # 37
for i in range(number_of_cells):
    # index: 0 is the center cell, the next cells spiral outwards
    # richness: 0 if the cell is unusable, 1-3 for usable cells
    # neigh_0: the index of the neighbouring cell for each direction
    index, richness, neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5 = [int(j) for j in input().split()]

    b = BoardCell(index, richness, neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5)
    game_board.add(b)

# game loop
while True:
    game_board.possible_actions.clear()
    game_board.list_index_occupied_cells.clear()

    day = int(input())  # the game lasts 24 days: 0-23
    game_board.set_today(day)
    sun_direction = compute_sun_direction(day)

    nutrients = int(input())  # the base score you gain from the next COMPLETE action
    game_board.set_nutrients(nutrients)

    # sun: your sun points (18 first turn)
    # score: your current score
    my_sun_points, my_score = [int(i) for i in input().split()]  #
    inputs = input().split()
    opp_sun = int(inputs[0])  # opponent's sun points
    opp_score = int(inputs[1])  # opponent's score
    opp_is_waiting = inputs[2] != "0"  # whether your opponent is asleep until the next day
    number_of_trees = int(input())  # the current amount of trees

    # my_trees = []
    # opp_trees = []

    is_level_three = -1  # is there a tree level 3?
    index_level_three = -1  # is yes, keep the index of the most valuable one
    max_level_three_points = -1

    my_forest = Forest()
    opp_forest = Forest()

    game_board.set_friendly_forest(my_forest)
    game_board.set_opp_forest(opp_forest)

    for i in range(number_of_trees):
        inputs = input().split()
        cell_index = int(inputs[0])  # location of this tree
        game_board.add_occupancy(cell_index)

        size = int(inputs[1])  # size of this tree: 0-3
        is_mine = inputs[2] != "0"  # 1 if this is your tree
        is_dormant = inputs[3] != "0"  # 1 if this tree is dormant

        cell_richness = game_board.get_cell(cell_index).richness
        points_if_chopped = -1  # Vous ne pouvez compléter le cycle de vie que des arbres de taille 3
        if size == 3:
            # Compléter le cycle de vie d'un arbre vous rapportera autant de points que
            # la valeur actuelle de nutriments (nutrient) + un bonus basé sur la richesse (richness) de la case
            points_if_chopped = nutrients
            if cell_richness == 2:
                points_if_chopped += 2
            elif cell_richness == 3:
                points_if_chopped += 4

        tree = Tree(tree_cell_index=cell_index,
                    tree_cell_obj=game_board.get_cell(cell_index),
                    tree_size=size,
                    tree_cell_richness=cell_richness,
                    tree_points_if_chopped=points_if_chopped,
                    tree_is_dormant=is_dormant,
                    tree_is_mine=is_mine)

        set_shaded_info(tree, board=game_board)

        if is_mine:
            # A MOI DICT TREE!
            my_forest.add(tree)

            # TODO: à mettre dans une fonction
            # Récupérer l'arbre qui donne le plus de points à chop chop
            if size == 3:
                if is_level_three == -1:
                    is_level_three = 1
                    index_level_three = len(my_forest) - 1
                    max_level_three_points = tree.points_if_chopped
                else:
                    if max_level_three_points < tree.points_if_chopped:
                        max_level_three_points = tree.points_if_chopped
                        index_level_three = len(my_forest) - 1
        else:
            opp_forest.add(tree)

    number_of_possible_actions = int(input())  # all legal actions

    for i in range(number_of_possible_actions):
        possible_action = input()
        game_board.possible_actions.append(Action.parse(possible_action))
        print(f"Day:{day} | Possible action:{possible_action}", file=sys.stderr, flush=True)

    # Update Tree, calcule les growing_interest et mets à jour les champs tree.cost_to_grow
    # Pourquoi ? Pour connaitre l'arbre qui rapporterait le plus de points à être grandi (par rapport à sa position)
    # TODO: à mettre dans une fonction
    index_growing = -1
    max_growing_points = -1
    for i in range(len(my_forest)):
        tree = my_forest.get(i)

        tree.set_cost_to_grow(my_forest.cost_to_grow(tree.size))

        growing_interest_points = tree.growing_interest()
        tree.set_interest(growing_interest_points)

        if max_growing_points < growing_interest_points:
            max_growing_points = growing_interest_points
            index_growing = i

    if DEBUG_PRINT:
        # print(f"Tree HIGH POINT: ({is_level_three})
        # {my_forest.get(index_level_three).cell_index}", file=sys.stderr, flush=True)
        # print(f"Tree TO GROW: {my_forest.get(index_growing).cell_index}", file=sys.stderr, flush=True)
        print(f"Today is: {day}", file=sys.stderr, flush=True)
        pretty_forest_print(my_forest)
        print(file=sys.stderr, flush=True)

        for tr in my_forest.all_trees:
            prettree_print(tr, board=game_board, sun_dir=sun_direction)
            print("", file=sys.stderr, flush=True)
            print(f"reachable seeds: {reachable_cells_to_seed(tr, game_board)}", file=sys.stderr, flush=True)

            best_to_seed_tree = best_to_seed_around_tree(tr, game_board)
            if best_to_seed_tree is not None:
                print(f"best to seed: {best_to_seed_tree.index}",
                      file=sys.stderr, flush=True)
                print(file=sys.stderr, flush=True)

    best_tree_cell_index, pts = my_forest.get_best_tree_interest()
    action = strategy_silver_2(board=game_board, my_forest=my_forest, sun_points=my_sun_points)

    # GROW cellIdx | SEED sourceIdx targetIdx | COMPLETE cellIdx | WAIT <message>
    print(action)
